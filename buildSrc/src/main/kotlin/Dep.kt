object Dep {
    const val kotlinVersion = "1.3.70"
    const val appcompatVersion = "1.1.0"
    const val coreKtxVersion = "1.3.0"
    const val lifecycleVersion = "2.3.0-alpha04"
}

object TestDep {
    const val junitVersion = "4.13"
    const val androidJunitVersion = "1.1.1"
    const val espressoCore = "3.2.0"
}