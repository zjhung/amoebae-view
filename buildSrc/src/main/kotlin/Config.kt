object Config {
    const val artifactName = "amoebae-view"
    const val versionCode = 1
    const val versionName= "0.0.1"

    const val compileSdk = 29
    const val targetSdk = 29
    const val minSdk = 24
}