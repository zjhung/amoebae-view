import com.android.build.gradle.api.LibraryVariant
import com.android.build.gradle.internal.api.LibraryVariantOutputImpl
import com.jfrog.bintray.gradle.BintrayExtension
import java.util.Properties

plugins {
    id("com.android.library")
    id("maven-publish")
    id("com.jfrog.bintray") version "1.8.4"
    kotlin("android")
    kotlin("android.extensions")
}

android {
    compileSdkVersion(Config.compileSdk)

    defaultConfig {
        minSdkVersion(Config.minSdk)
        targetSdkVersion(Config.targetSdk)
        versionCode = Config.versionCode
        versionName = Config.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    setProperty("archivesBaseName", Config.artifactName)

    buildTypes {
        named("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
}

android.libraryVariants.all {
    generateAarName(this)
}

fun generateAarName(variant: LibraryVariant) {
    variant.outputs.filterIsInstance<LibraryVariantOutputImpl>().forEach { output ->
        output.outputFileName = "${Config.artifactName}-${variant.name}-${Config.versionName}.aar"
    }
}

dependencies {
    implementation(fileTree( mapOf("dir" to "libs", "include" to listOf("*.jar")) ))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dep.kotlinVersion}")
    implementation("androidx.appcompat:appcompat:${Dep.appcompatVersion}")
    implementation("androidx.core:core-ktx:${Dep.coreKtxVersion}")
    testImplementation("junit:junit:${TestDep.junitVersion}")
    androidTestImplementation("androidx.test.ext:junit:${TestDep.androidJunitVersion}")
    androidTestImplementation("androidx.test.espresso:espresso-core:${TestDep.espressoCore}")
}

val sourceJar = task("sourceJar", Jar::class) {
    archiveClassifier.set("sources")
    from(android.sourceSets.getByName("main").java.srcDirs)
}

publishing {
    publications {
        create<MavenPublication>("AmoebaePublication") {
            groupId = "zj.ui"
            artifactId = Config.artifactName
            version = Config.versionName
            artifact(sourceJar)
            artifact("$buildDir/outputs/aar/${Config.artifactName}-release-${Config.versionName}.aar")

            pom.withXml {
                val dependenciesNode = asNode().appendNode("dependencies")

                project.configurations.implementation.get().allDependencies.forEach {
                    if (it.group != null) {
                        val dependencyNode = dependenciesNode.appendNode("dependency")
                        dependencyNode.appendNode("groupId", it.group)
                        dependencyNode.appendNode("artifactId", it.name)
                        dependencyNode.appendNode("version", it.version)

                        (it as? ModuleDependency)?.let { dep ->
                            if (!dep.excludeRules.isNullOrEmpty()) {
                                val exclusionsNode = dependencyNode.appendNode("exclusions")
                                dep.excludeRules.forEach { rule ->
                                    val exclusionNode = exclusionsNode.appendNode("exclusion")
                                    exclusionNode.appendNode("groupId", rule.group)
                                    exclusionNode.appendNode("artifactId", rule.module)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    repositories {
        maven("https://dl.bintray.com/imuniquezj/android")
    }
}

fun findProperty(s: String) = project.findProperty(s) as String?

val properties = Properties()
properties.load(project.rootProject.file("local.properties").inputStream()) // load local.properties
val bintrayUser: String? = properties.getProperty("bintray.username")
val bintrayApiKey: String? = properties.getProperty("bintray.apiKey")

bintray {
    user = findProperty("bintrayUser") ?: bintrayUser
    key = findProperty("bintrayApiKey") ?: bintrayApiKey
    publish = true
    setPublications("AmoebaePublication")
    pkg(delegateClosureOf<BintrayExtension.PackageConfig> {
        repo = "android"
        name = Config.artifactName
        publicDownloadNumbers = true
        vcsUrl = "https://zjhung@bitbucket.org/zjhung/amoebae-view.git"
        setLabels("android", "kotlin")
        setLicenses("Apache-2.0")
        version(delegateClosureOf<BintrayExtension.VersionConfig> {
            name = Config.versionName
        })
    })
}