package zj.ui.amoebae.ext

import android.content.res.Resources

fun Float.dpToPx(): Float {
    return this * Resources.getSystem().displayMetrics.density
}