package zj.ui.amoebae.ext

import android.graphics.PointF
import android.view.View

val View.center get() = PointF(x + width / 2f, y + height / 2f)