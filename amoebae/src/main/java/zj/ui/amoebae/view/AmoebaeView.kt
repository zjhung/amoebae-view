package zj.ui.amoebae.view

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.FloatProperty
import android.util.IntProperty
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import zj.ui.amoebae.R
import zj.ui.amoebae.ext.center
import zj.ui.amoebae.ext.dpToPx
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin
import kotlin.random.Random

class AmoebaeView: View {

    enum class MoveStyle {
        WAVE, WORM
    }

    private lateinit var paint: Paint
    private val path = Path()
    private val randomMap = mutableMapOf<Double, Float?>()
    private val positiveOrNegativeMap = mutableMapOf<Double, Int>()
    private val angleRadiusMap = mutableMapOf<Double, Float>()
    private var animator: ValueAnimator? = null

    var moveStyle: MoveStyle = MoveStyle.WAVE
    var circleRadius = 120f.dpToPx()
    var adjust = 1
    var steps = 30
    var antenna = 12
    var color: Int = Color.WHITE

    companion object {
        private val minRadius = 65f.dpToPx()
        private val maxRadius = 180f.dpToPx()
        private const val PROPERTY_RADIUS = "radius"
        private const val PROPERTY_ADJUST = "adjust"
        private const val PROPERTY_STEP = "step"
        private const val PROPERTY_ANTENNA = "antenna"

        val RADIUS = object: FloatProperty<AmoebaeView>(PROPERTY_RADIUS) {
            override fun setValue(amoebae: AmoebaeView, value: Float) {
                amoebae.circleRadius = value.coerceAtLeast(minRadius)
            }
            override fun get(amoebae: AmoebaeView): Float {
                return amoebae.circleRadius
            }
        }

        val ADJUST = object: IntProperty<AmoebaeView>(PROPERTY_ADJUST) {
            override fun setValue(amoebae: AmoebaeView, value: Int) {
                amoebae.adjust = value
            }
            override fun get(amoebae: AmoebaeView): Int {
                return amoebae.adjust
            }
        }

        val ANTENNA = object: IntProperty<AmoebaeView>(PROPERTY_ANTENNA) {
            override fun setValue(amoebae: AmoebaeView, value: Int) {
                amoebae.antenna = value.coerceAtLeast(3)
            }
            override fun get(amoebae: AmoebaeView): Int {
                return amoebae.antenna
            }
        }
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {
        applyAttributes(attrs)
        init()
    }

    override fun onDraw(canvas: Canvas) {
        when (moveStyle) {
            MoveStyle.WAVE -> drawWave(canvas)
            MoveStyle.WORM -> drawWorm(canvas)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        val cp = center
        paint.shader = RadialGradient(cp.x, cp.y, circleRadius,
            intArrayOf(color, Color.TRANSPARENT), floatArrayOf(0f, 1f), Shader.TileMode.CLAMP)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        animator?.cancel()
    }

    fun startMove(style: MoveStyle = MoveStyle.WORM, radiusInDp: Float? = 120f, antennaCount: Int? = 12) {
        animator?.cancel()
        moveStyle = style
        val finalRadius = radiusInDp?.dpToPx()?.let { minRadius + it } ?: circleRadius
        val cp = center
        circleRadius = finalRadius
        antenna = antennaCount ?: antenna
        paint.shader = RadialGradient(cp.x, cp.y, finalRadius,
            intArrayOf(color, Color.TRANSPARENT), floatArrayOf(0f, 1f), Shader.TileMode.CLAMP)

        val radiusProp = PropertyValuesHolder.ofFloat(RADIUS, minRadius, finalRadius)
        animator = ValueAnimator().apply {
            setValues(radiusProp)
            duration = 3000
            repeatMode = ValueAnimator.REVERSE
            repeatCount = ValueAnimator.INFINITE
            interpolator = AccelerateDecelerateInterpolator()
            addUpdateListener {
                invalidate()
            }
            start()
        }
    }

    fun stopMove() {
        animator?.cancel()
    }

    private fun applyAttributes(attrs: AttributeSet?) {
        context.theme.obtainStyledAttributes(attrs,
            R.styleable.AmoebaeView, 0, 0).apply {
            try {
                color = getColor(R.styleable.AmoebaeView_color, color)
                val styleIndex = getInt(R.styleable.AmoebaeView_moveStyle, 0)
                moveStyle = MoveStyle.values()[styleIndex]
                antenna = getInt(R.styleable.AmoebaeView_antenna, antenna)
            } finally {
                recycle()
            }
        }
    }

    private fun init() {
        paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            maskFilter = BlurMaskFilter(10f.dpToPx(), BlurMaskFilter.Blur.NORMAL)
            style = Paint.Style.FILL
            strokeJoin = Paint.Join.ROUND
            strokeCap = Paint.Cap.ROUND
            pathEffect = CornerPathEffect(circleRadius)
        }
    }

    private fun drawWave(canvas: Canvas) {
        val cp = center

        path.reset()
        val randomRadius = computeRadius(0.0, circleRadius)
        val startX = cp.x + randomRadius * cos(0.0).toFloat()
        val startY = cp.y + randomRadius * sin(0.0).toFloat()
        path.moveTo(startX, startY)

        for (angle in steps..360 step steps) {
            val randomRadius1 = computeRadius(angle.toDouble(), circleRadius)
            val end1X = cp.x + randomRadius1 * cos(-angle.toDouble()).toFloat()
            val end1Y = cp.y + randomRadius1 * sin(-angle.toDouble()).toFloat()
            path.lineTo(end1X, end1Y)
        }

        path.close()
        canvas.drawPath(path, paint)
    }

    private fun drawWorm(canvas: Canvas) {
        val cp = center

        path.reset()
        val randomRadius = computeRadius(0.0, circleRadius)
        val startX = cp.x + randomRadius * cos(0.0).toFloat()
        val startY = cp.y + randomRadius * sin(0.0).toFloat()
        path.moveTo(startX, startY)

        val angleInRadians = 2f * PI / antenna
        for (index in 1 until antenna) {
            val randomRadius1 = computeRadius((angleInRadians * index), circleRadius)
            val end1X = cp.x + randomRadius1 * cos(-(angleInRadians * index)).toFloat()
            val end1Y = cp.y + randomRadius1 * sin(-(angleInRadians * index)).toFloat()
            path.lineTo(end1X, end1Y)
        }

        path.close()
        canvas.drawPath(path, paint)
    }

    private fun computeRadius(angle: Double, inRadius: Float): Float {
        if (randomMap[angle] == null) {
            randomMap[angle] = Random.nextInt(6, 11) / 10f
        }
        if (angleRadiusMap[angle] == null) {
            angleRadiusMap[angle] = inRadius * randomMap[angle]!! + adjust * positiveOrNegative(angle)
        }
        var newRadius = angleRadiusMap[angle]!!
        if (newRadius < minRadius || newRadius > maxRadius) {
            reversePositiveOrNegative(angle)
        }
        newRadius += positiveOrNegative(angle)
        angleRadiusMap[angle] = newRadius
        return newRadius
    }

    private fun positiveOrNegative(angle: Double): Int {
        if (positiveOrNegativeMap[angle] == null) {
            positiveOrNegativeMap[angle] = when (Random.nextInt() % 2) {
                0 -> -1
                else -> 1
            }
        }
        return positiveOrNegativeMap[angle]!!
    }

    private fun reversePositiveOrNegative(angle: Double) {
        positiveOrNegativeMap[angle] = positiveOrNegativeMap[angle]!! * -1
    }
}