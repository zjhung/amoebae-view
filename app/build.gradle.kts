
plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}

android {
    compileSdkVersion(Config.compileSdk)
    buildFeatures.viewBinding = true

    defaultConfig {
        applicationId = "zj.view.amoebae"
        minSdkVersion(Config.minSdk)
        targetSdkVersion(Config.targetSdk)
        versionCode = Config.versionCode
        versionName = Config.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        named("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

}

dependencies {
    implementation(project(":amoebae"))
    implementation(fileTree( mapOf("dir" to "libs", "include" to listOf("*.jar")) ))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dep.kotlinVersion}")
    implementation("androidx.appcompat:appcompat:${Dep.appcompatVersion}")
    implementation("androidx.core:core-ktx:${Dep.coreKtxVersion}")
    implementation("androidx.fragment:fragment-ktx:1.3.0-alpha05")
    implementation("androidx.constraintlayout:constraintlayout:1.1.3")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:${Dep.lifecycleVersion}")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:${Dep.lifecycleVersion}")
    kapt("androidx.lifecycle:lifecycle-compiler:${Dep.lifecycleVersion}")

    testImplementation("junit:junit:${TestDep.junitVersion}")
    androidTestImplementation("androidx.test.ext:junit:${TestDep.androidJunitVersion}")
    androidTestImplementation("androidx.test.espresso:espresso-core:${TestDep.espressoCore}")
}
