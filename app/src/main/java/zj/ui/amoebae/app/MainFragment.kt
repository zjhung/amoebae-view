package zj.ui.amoebae.app

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.lifecycle.ViewModelProvider
import zj.ui.amoebae.app.databinding.MainFragmentBinding
import zj.ui.amoebae.ext.dpToPx
import zj.ui.amoebae.view.AmoebaeView

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private var _vb: MainFragmentBinding? = null
    private val vb get() = _vb!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _vb = MainFragmentBinding.inflate(inflater, container, false)
        return vb.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        vb.btnStart.setOnClickListener {
            val style = when (vb.radioGroup.checkedRadioButtonId) {
                R.id.rbStyleWave -> AmoebaeView.MoveStyle.WAVE
                else -> AmoebaeView.MoveStyle.WORM
            }
            vb.amoebaeView.startMove(style, vb.sbRadius.progress + 65f, vb.sbAntenna.progress + 3)
        }
    }
}
